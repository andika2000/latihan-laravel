@extends('templates/index')

@section('title')
    Halaman Utama
@endsection

@section('body')
    <div class="container">

        <a class="btn btn-primary mt-5" href="{{url('create')}}">Tambah Data</a>

        <table class="table table-striped mt-2">
            <thead>
                <tr>
                <th scope="col">No</th>
                <th scope="col">Nama Produk</th>
                <th scope="col">Keterangan</th>
                <th scope="col">Harga</th>
                <th scope="col">Jumlah</th>
                <th scope="col">Aksi</th>
                </tr>
            </thead>
            <tbody>
                @foreach($data as $d)
                    <tr>
                        <th scope="row">{{$loop->iteration}}</th>
                        <td>{{$d->nama_produk}}</td>
                        <td>{{$d->keterangan}}</td>
                        <td>{{$d->harga}}</td>
                        <td>{{$d->jumlah}}</td>
                        <td>
                            <a class="btn btn-success btn-sm mr-2" href="#">Edit</a>
                            <form action="/home/{{$d->id}}" method="post">
                                @method('delete')
                                @csrf
                                <button class="btn btn-danger btn-sm mr-2" type="submit">Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection